import Vue from 'vue'
import App from './App.vue'
import jQuery from  'jquery'
import '@/assets/css/tailwind.css'
import Vuex from 'vuex'
import store from "@/store";
import { firestorePlugin } from 'vuefire';


Vue.use(Vuex)
const $ = jQuery

Vue.config.productionTip = false
Vue.use(firestorePlugin)

new Vue({
  store,
  $,
  render: h => h(App),
}).$mount('#app')
